enum HttpMethod {
  GET,
  POST,
  PUT,
  DELETE,
}

export default HttpMethod;
