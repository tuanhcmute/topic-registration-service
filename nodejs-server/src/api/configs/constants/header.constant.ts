const ACCESS_CONTROL_ALLOW_ORIGIN = "Access-Control-Allow-Origin";
const ACCESS_CONTROL_ALLOW_METHODS = "Access-Control-Allow-Methods";
const ACCESS_CONTROL_ALLOW_HEADERS = "Access-Control-Allow-Headers";
const CONTENT_TYPE = "Content-Type";
const AUTHORIZATION = "Authorization";

export {
  ACCESS_CONTROL_ALLOW_ORIGIN,
  ACCESS_CONTROL_ALLOW_METHODS,
  ACCESS_CONTROL_ALLOW_HEADERS,
  CONTENT_TYPE,
  AUTHORIZATION,
};
