enum TopicStatus {
  APPROVED = "APPROVED",
  REJECTED = "REJECTED",
  UPDATED = "UPDATED",
  PENDING = "PENDING",
  ASSIGNED = "ASSIGNED",
}

export default TopicStatus;
