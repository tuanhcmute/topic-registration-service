enum SemesterStatus {
  ACTIVATED = "ACTIVATED",
  TERMINATED = "TERMINATED",
  SCHEDULED = "SCHEDULED",
}

export default SemesterStatus;
