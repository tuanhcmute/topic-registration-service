export { default as authController } from "./auth.controller";
export { default as enrollmentPeriodController } from "./enrollmentPeriod.controller";
export { default as userController } from "./user.controller";
export { default as topicController } from "./topic.controller";
export { default as approvalHistoryController } from "./approvalHistory.controller";
export { default as divisionController } from "./division.controller";
