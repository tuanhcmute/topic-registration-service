class ErrorMessages {
  public static USER_NOT_FOUND = "User not found - ";
  public static USER_ALREADY_EXISTS = "User already exists - ";
  public static INTERNAL_SERVER_ERROR = "Internal server error - ";
  public static UNAUTHORIZED = "Unauthorized - please log in -";
  public static INVALID_TOKEN = "Invalid token - please log in -";
}

export default ErrorMessages;
