import { ApprovalHistoryInstance } from "@models";

export interface IListApprovalHistoryReponse {
  approvalHistories?: ApprovalHistoryInstance[];
}
