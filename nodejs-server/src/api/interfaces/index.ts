export { default as IRoutes } from "./routes.interface";
export { default as IResponseModel } from "./responseModel.interface";
export * from "./responseModel.interface";
export * from "./user.interface";
export * from "./enrollmentPeriod.interface";
export * from "./approvalHistory.interface";
export * from "./topic.interface";
export * from "./division.interface";
