import { EnrollmentPeriodInstance } from "@models";

export interface IEnrollmentPeriodResponse {
  enrollmentPeriod?: EnrollmentPeriodInstance;
}
