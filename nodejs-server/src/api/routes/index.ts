export { default as authRoutes } from "./auth.routes";
export { default as enrollmentPeriodRoutes } from "./enrollmentPeriod.routes";
export { default as userRoutes } from "./user.routes";
export { default as topicRoutes } from "./topic.routes";
export { default as approvalHistoryRoutes } from "./approvalHistory.routes";
export { default as divisionRoutes } from "./division.routes";
