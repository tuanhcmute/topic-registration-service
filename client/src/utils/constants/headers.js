const headers = {
  CONTENT_TYPE: "Content-Type",
  APPLICATION_JSON: "application/json",
  APPLICATION_FORM_URLENCODED_VALUE: "application/x-www-form-urlencoded",
};

export default headers;
