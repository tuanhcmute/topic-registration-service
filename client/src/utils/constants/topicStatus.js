const topicStatus = {
  created: {
    value: "CREATED",
    label: "Đã được tạo",
  },
  pending: {
    value: "PENDING",
    label: "Đang chờ duyệt",
  },
  approved: {
    value: "APPROVED",
    label: "Đã được duyệt",
  },
  rejected: {
    value: "REJECTED",
    label: "Chưa được duyệt",
  },
  all: {
    value: "ALL",
    label: "Tất cả",
  },
  updated: {
    value: "UPDATED",
    label: "Đã cập nhật",
  },
};

export default topicStatus;
