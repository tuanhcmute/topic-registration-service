export const ROLE_ANONYMOUS = "ROLE_ANONYMOUS";
export const ROLE_STUDENT = "ROLE_STUDENT";
export const ROLE_LECTURE = "ROLE_LECTURE";
export const ROLE_HEAD = "ROLE_HEAD";
export const ROLE_ADMIN = "ROLE_ADMIN";
