const paths = {
  HOME: "/",
  PROFILE: "/profile",
  LOGIN: "/login",
  LOGOUT: "/logout",
};

export default paths;
