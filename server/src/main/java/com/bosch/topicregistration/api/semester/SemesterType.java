package com.bosch.topicregistration.api.semester;

public enum SemesterType {
    FIRST_SEMESTER,
    SECOND_SEMESTER,
    THIRD_SEMESTER
}
