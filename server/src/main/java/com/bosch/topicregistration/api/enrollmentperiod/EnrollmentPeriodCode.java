package com.bosch.topicregistration.api.enrollmentperiod;

public enum EnrollmentPeriodCode {
    LECTURE_ENROLLMENT_PERIOD,
    STUDENT_ENROLLMENT_PERIOD,
}
