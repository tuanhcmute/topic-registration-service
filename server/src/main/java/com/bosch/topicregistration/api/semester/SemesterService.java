package com.bosch.topicregistration.api.semester;

import java.util.Optional;

public interface SemesterService {
    Semester getActivatedSemester();
}
