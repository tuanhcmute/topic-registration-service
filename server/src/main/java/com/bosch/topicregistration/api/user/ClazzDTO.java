package com.bosch.topicregistration.api.user;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ClazzDTO {
    private String code;
    private String description;
}
