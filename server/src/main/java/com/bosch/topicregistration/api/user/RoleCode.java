package com.bosch.topicregistration.api.user;

public enum RoleCode {
    ROLE_ANONYMOUS,
    ROLE_STUDENT,
    ROLE_LECTURE,
    ROLE_HEAD,
    ROLE_ADMIN
}
