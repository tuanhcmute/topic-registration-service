package com.bosch.topicregistration.api.topic;

public enum TopicType {
    TLCN,
    KLTN
}
