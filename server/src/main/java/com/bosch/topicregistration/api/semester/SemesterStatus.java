package com.bosch.topicregistration.api.semester;

public enum SemesterStatus {
    ACTIVATED,
    TERMINATED,
    SCHEDULED
}
