package com.bosch.topicregistration.api.user;

import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MajorDTO {
    private String code;
    private String name;
}
