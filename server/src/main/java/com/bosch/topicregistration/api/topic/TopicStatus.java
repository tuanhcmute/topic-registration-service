package com.bosch.topicregistration.api.topic;

public enum TopicStatus {
    APPROVED,
    REJECTED,
    UPDATED,
    PENDING,

}
