package com.bosch.topicregistration.api.user;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class LectureDTO {
    private String ntid;
    private String name;
}
